﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OcpDockerGitlab.Controllers
{
    [ApiController]
    [Route("api")]
    public class VersionController : ControllerBase
    {
        private static readonly int version = 2;

        [HttpGet("version")]
        public IActionResult GetVersion()
        {
            return Ok($"Api version: {version} : OS Version: {System.Environment.OSVersion}");
        }
    }
}
